# Environment setup

## Devbox

This repo provides a config file for [`devbox`](https://www.jetpack.io/devbox/) which is built on top of nix to make
management of development environments more reproducible and secluded. See `devbox.json` for details on what
packages, environment variables and scripts are used in this project.

## Authentication

Regardless of whether `devbox` is used or not, authentication for terraform and the application deployed with k8s
needs to be provided. The project is set up such that a JSON file named `zeit-bewerbung-key.json` containing
proper credentials is placed in the project root directory. It is recommended to create an appropriately
scoped service user for this purpose that has the necessary permissions. These need to include creating and removing
buckets and creating and deleting objects from buckets.

Further, the terraform setup expects the environment variable `GOOGLE_APPLICATION_CREDENTIALS` to be set
and containing the path to the beforementioned JSON file. If `devbox` is used, this is set automatically.

# Usage

## The app image

The kubernetes config is set up to use an image called `coriakin/app:latest` which is located in my personal
account on docker.io. To change this, edit `k8s/deploy.yaml` accordingly. If you want to use a local image,
you need to make it available to minikube. Depending on your docker version, there are several approaches to
this, the one I used is having minikube build the image itself with `minikube image build -t app -f Dockerfile .`
from within the `app` directory.

## Automatic setup using devbox

Within the `devbox.json` file there are a few defined commands that can be executed with `devbox run $COMMAND`.
To set everything up automatically run `devbox run start`. This will create the GCS resources, a minikube cluster,
a namespace and deploy the app container.

## Manual setup

### Terraform

Simply navigate to the "terraform directory" and execute `terraform plan` and `terraform apply` to create the relevant resources.
If desired the various inputs can be changed in `terraform/main.tf`.

### Kubernetes

#### Cluster setup

Before the application can be deployed the image needs to be made available to `minikube`. By default images are pulled from dockerhub
and the default image configured has been pushed there so no actions need to be taken. If, however, a locally built image should be used,
extra steps must be taken. These depend on the docker version in use, the most robust way appears to be to have `minikube` build the image
itself. This can be achieved with `minikube image build -t app -f Dockerfile .` from within the `app` directory.

After the image is taken care of you can start a local cluster with `minikube start`.

#### Deploy application

1. Navigate to the `k8s` directory
2. Create the secret containing the credentials for authenticating with GCS with `kubectl create secret generic google-creds --from-file ../zeit-bewerbung-key.json`
3. Use `kubectl apply -f ${Path_to_config}` (for `namespace.yaml` and `deploy.yaml`) from within the `k8s` directory to apply the respective configuration
4. (Optional) To make life easier use `kubens` to switch to the target namespace
5. Verify everything runs as expected with `kubectl logs zeit-app-${ORDINAL_NUMBER}` with the ordinal number being between 0 and 2, if not changed.
