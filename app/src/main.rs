use object_store::{
    gcp::{GoogleCloudStorage, GoogleCloudStorageBuilder},
    path::Path,
    ObjectStore
};
use std::{env, sync::Arc, time, time::Instant};
use tokio::time::sleep;
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;
use uuid::Uuid;

#[tokio::main]
async fn main() {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    
    let Ok(creds) = env::var("GOOGLE_APPLICATION_CREDENTIALS") else {
        println!("Path to credentials not set!");
        std::process::exit(1);
    };
    let Ok(bucket_name) = env::var("BUCKET_NAME") else {
        println!("Bucket name environment variable not set!");
        std::process::exit(1);
    };
    
    let Some(arg) = env::args().nth(1) else {
        println!("Please give a file name to be downloaded as argument!");
        std::process::exit(1);
    };
    let path = Path::from(arg);
    let ten_secs = time::Duration::from_secs(10);
    
    loop {
        let Ok(gcs) = GoogleCloudStorageBuilder::new()
            .with_application_credentials(&creds)
            .with_bucket_name(&bucket_name)
            .build()
            else {
                println!("Creating GCS object failed. Are the credentials properly configured?");
                std::process::exit(1);
            };
        let start = Instant::now();
        let object_content = get_object(gcs, &path).await;
        let duration = start.elapsed().as_millis().to_string() + " ms";
        let uuid = Uuid::new_v4().to_string();

        info!(uuid, duration, object_content);
        sleep(ten_secs).await;
    }
}

async fn get_object(gcs: GoogleCloudStorage, path: &Path) -> String {
    let object_store: Arc<dyn ObjectStore> = Arc::new(gcs);
    let Ok(result) = object_store.get(path).await else {
        println!("Error while retrieving object!");
        std::process::exit(1);
    };
    let Ok(object) = result.bytes().await else {
        println!("Error reading bytes from file!");
        std::process::exit(1);
    };
    let Ok(object_content) = std::str::from_utf8(&object) else {
        println!("Error! Object does not contain valid UTF-8!");
        std::process::exit(1);
    };
    
    object_content.to_owned()
}
