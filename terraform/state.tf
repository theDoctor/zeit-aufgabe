provider "google" {
  project = "zeit-bewerbung"
  region  = "EU"
}

resource "google_storage_bucket" "terraform-state-bucket" {
  name                        = "tf-state-bucket-eu"
  location                    = "EU"
  force_destroy               = false
  public_access_prevention    = "enforced"
  uniform_bucket_level_access = true
  storage_class               = "STANDARD"
  versioning {
    enabled = true
  }
}

