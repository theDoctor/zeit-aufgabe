terraform {
  backend "gcs" {
    bucket = "tf-state-bucket-eu"
    prefix = "terraform/state"
  }
}

