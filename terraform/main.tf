module "create-GCS-bucket" {
  source      = "./modules/create-GCS-bucket"
  bucket_name = "zeit-test-bucket"
  upload_object = [
    {
      name = "lorem_ipsum.txt"
      path = "files/lorem_ipsum.txt"
    },
    {
      name = "test.json"
      path = "files/test.json"
    },
    {
      name = "test.yaml"
      path = "files/test.yaml"
    }
  ]
}
