variable "project_id" {
  default     = "zeit-bewerbung"
  type        = string
  description = "Project ID where the bucket will be created"
}

variable "project_region" {
  default     = "us-central1"
  type        = string
  description = "Provider region"
}

variable "bucket_name" {
  type        = string
  description = "Name of the bucket to be created"
  nullable    = false
  validation {
    condition     = !contains(["google", "g00gle"], lower(var.bucket_name)) && length(var.bucket_name) < 222 && can(regex("^([0-9a-zA-Z_.-]{3,63})([.])?([0-9a-zA-Z_.-]{3,63})?$", var.bucket_name)) && !can(regex("^goog", var.bucket_name))
    error_message = "Invalid bucket name"
  }
}

variable "upload_object" {
  description = "List of objects (containing name and path on local filesystem) to be uploaded"
  nullable    = true
  type = list(object({
    name = string
    path = string
  }))
  validation {
    condition = alltrue([
      for obj in var.upload_object : !can(regex("^[.]well-known/acme-challenge/", obj.name)) && !can(regex("^[.]{1,2}$", obj.name))
    ])
    error_message = "Invalid object name"
  }
}
