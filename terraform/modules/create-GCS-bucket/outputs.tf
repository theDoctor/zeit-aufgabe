output "bucket_id" {
  description = "ID of the created bucket"
  value       = google_storage_bucket.zeit_bucket.id
}

output "object_ids" {
  value = { for k, obj in google_storage_bucket_object.default : k => obj.id }
}

