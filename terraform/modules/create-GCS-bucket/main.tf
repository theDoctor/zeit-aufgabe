provider "google" {
  project = var.project_id
  region  = var.project_region
}

resource "google_storage_bucket" "zeit_bucket" {
  name                        = var.bucket_name
  location                    = "EU"
  storage_class               = "STANDARD"
  uniform_bucket_level_access = true
  force_destroy               = true
  versioning {
    enabled = true
  }
}

resource "google_storage_bucket_object" "default" {
  for_each = { for i, obj in var.upload_object : i => obj }
  name     = each.value.name
  source   = each.value.path
  bucket   = google_storage_bucket.zeit_bucket.id
}
